# Задача:Створіть дві змінні first=10, second=30.
# Виведіть на екран результат математичної взаємодії
# (+, -, *, / и тд.) для цих чисел.
first = 10
second = 30
print('first + second =', first + second)
print('first - second =', first - second)
print('first * second =', first * second)
print('first / second =', first / second)
print('first // second =', first // second)
print('first % second =', first % second)
print('first ** second =', first ** second)


# Задача: Створіть змінну і по черзі запишіть в неї результат порівняння
# (<, > , ==, !=) чисел з завдання 1. Виведіть на екран результат кожного
# порівняння.
first = 10
second = 30

less = first < second
greater = first > second
equal = first == second
non_equal = first != second

print('less ->', less )
print('greater ->', greater)
print('equal->', equal)
print('not equal->', non_equal)

