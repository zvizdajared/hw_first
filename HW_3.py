# 1.Напишіть цикл, який буде вимагати від
# користувача ввести слово, в якому є буква "о"
# (враховуються як великі так і маленькі). Цикл не повинен
# завершитися, якщо користувач ввів слово без букви "о".

word = ''
while 'o' not in word and 'о' not in word: #шоб проверить и кирилицу
    word = input('Введіть будь яке слово ->').lower()

# Є list з даними lst1 = ['1', '2', 3, True, 'False',
# 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum']. Напишіть код,
# який свормує новий list (наприклад lst2), який містить лише
# змінні типу стрінг, які присутні в lst1. Зауважте, що lst1
# не є статичним і може формуватися динамічно від запуску до
# запуску.

lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum']
lst2 = []
for i in lst1:
    if type(i) == str:
        lst2.append(i)

print(lst2)

# Є стрінг з певним текстом (можна скористатися input або константою).
# Напишіть код, який визначить кількість слів в цьому тексті, які закінчуються
# на "о" (враховуються як великі так і маленькі).
amount = 0

for i in input('ВВедіть будь яке слово ->').lower().split():
    if i[-1] == 'o' or i[-1] == 'о':   # if i.endswith('o'):
        amount += 1
print(amount)
